import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.learning.DataSet;
import org.neuroph.core.learning.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TrainingSetImport;
import org.neuroph.util.TransferFunctionType;


/**
 *
 * @author Ivana Lukic
 */
public class Winec{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String trainingSetFileName = "WineClassification.txt";
        String trainingSetFileName2 = "WineClassification2.txt";
        int inputsCount = 13;
        int outputsCount = 3;

        System.out.println("Running Sample");
        System.out.println("Using training set " + trainingSetFileName);

        // create training set
        DataSet trainingSet = null;
        try {
            trainingSet = TrainingSetImport.importFromFile(trainingSetFileName, inputsCount, outputsCount, ",");
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException ex) {
            System.out.println("Error reading file or bad number format!");
        } catch (NumberFormatException ex) {
            System.out.println("Error reading file or bad number format!");
        }


        // create multi layer perceptron
        System.out.println("Creating neural network");
        MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 13, 4, 3);

        // set learning parametars
        MomentumBackpropagation learningRule = (MomentumBackpropagation) neuralNet.getLearningRule();
        learningRule.setLearningRate(0.4);
        learningRule.setMomentum(0.6);

        // learn the training set
        System.out.println("Training neural network...");
        neuralNet.learn(trainingSet);
        System.out.println("Done!");

        // test perceptron
        System.out.println("Testing trained neural network");
        DataSet trainingSet2 = null;
        try {
            trainingSet2 = TrainingSetImport.importFromFile(trainingSetFileName2, inputsCount, outputsCount, ",");
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException ex) {
            System.out.println("Error reading file or bad number format!");
        } catch (NumberFormatException ex) {
            System.out.println("Error reading file or bad number format!");
        }
        testWineClassification(neuralNet, trainingSet2);


    }

    public static void testWineClassification(NeuralNetwork nnet, DataSet dset) {
        int k=1;

        for (DataSetRow trainingElement : dset.getRows()) {

            nnet.setInput(trainingElement.getInput());
            nnet.calculate();
            double[] networkOutput = nnet.getOutput();
            System.out.print(k++);
            System.out.print("Input: " + Arrays.toString(trainingElement.getInput())+"original output"+Arrays.toString(trainingElement.getDesiredOutput()));
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }

    }
}